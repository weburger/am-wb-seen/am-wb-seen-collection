/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('ngMaterialWeburgerSeenCollection')
/**
 * 
 * @description List all items of a collection and add into the scope as an item list.
 * 
 * 
 */
.controller('AmWbSeenCollectionCollection2Ctrl', function($scope, $wbUi, $collection, PaginatorParameter) {
	$scope.ctrl = {};
	var ctrl = $scope.ctrl;
	var wbModel = $scope.wbModel;
	var requests;
	var paginatorParameter = new PaginatorParameter();


	/**
	 * Load next page of documents
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		ctrl.collection.documents(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'relax';
		}, function() {
			ctrl.status = 'fail';
		});
	}
	/**
	 * Refresh all settings
	 * @returns
	 */
	function refresh() {
		ctrl.items = [];
		return nextPage();
	}


	/**
	 * Adding new event
	 * 
	 * @returns
	 */
	function newEvent(){
		return $wbUi.openDialog({
			controller : 'AmWbCollectionModelDialogCtrl',
			templateUrl : 'views/am-wb-collection-dialogs/event.html',
			parent : angular.element(document.body),
			clickOutsideToClose : true,
			fullscreen : true,
			locals : {
				model: {},
				style: {}
			},
		})//
		.then(function(event){
			return ctrl.collection.newDocument(event);
		})//
		.then(function(doc){
			ctrl.items.push(doc);
		});
	}
	
	/**
	 * Remove event form list and server
	 * @param event
	 * @returns
	 */
	function deleteEvent(event){
		return event.remove()//
		.then(function(){
			var index = ctrl.items.indexOf(event);
			if (index > -1) {
				ctrl.items .splice(index, 1);
			}
		});
	}
	
	/**
	 * Edit event
	 * 
	 * @param event
	 * @returns
	 */
	function editEvent(event){
		return $wbUi.openDialog({
			controller : 'AmWbCollectionModelDialogCtrl',
			templateUrl : 'views/am-wb-collection-dialogs/event.html',
			parent : angular.element(document.body),
			clickOutsideToClose : true,
			fullscreen : true,
			locals : {
				model: event,
				style: {}
			},
		})//
		.then(function(eventNew){
			return eventNew.update();
		});
	}

	/*
	 * Watch collection name change
	 */
	$scope.$watch('wbModel.collection', function(collectionName){
		$collection.collection(collectionName)//
		.then(function(collection){
			ctrl.collection = collection;
			return refresh();
		});
	});
	
	$scope.newEvent = newEvent;
	$scope.editEvent = editEvent;
	$scope.deleteEvent = deleteEvent;
});