/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerSeenCollection')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.run(function($widget) {
	$widget.newWidget({
		type: 'CollectionDocumentList',
		templateUrl : 'views/am-wb-collection-widgets/document-list.html',
		label : 'Document listt',
		description : 'Lists all document of a collection with a template.',
		image : 'images/wb/html.svg',
		help : 'https://gitlab.com/weburger/am-wb-seen-collection/wikis/home',
		controller: 'AmWbSeenCollectionCollectionCtrl',
		setting:['text' ],
		data:{
			template:{}
		}
	});
	$widget.newWidget({
		type: 'CollectionEventList',
		templateUrl : 'views/am-wb-collection-widgets/event-list.html',
		label : 'Event listt',
		description : 'List of all events.',
		icon : 'events',
		help : 'https://gitlab.com/weburger/am-wb-seen-collection/wikis/home',
		controller: 'AmWbSeenCollectionCollection2Ctrl',
		setting:['text', 'collection'],
		data:{
			template:{}
		}
	});
});
